mod structs;
use structs::{process_transfer, Stock};

fn use_structs() {
    let mut tuxedo = Stock::new("Tuxedo", 100.0).with_take_profit(2.0);
    let mut ms = Stock::new("Microsoft", 0.000001).with_stop_loss(1.0);
    let mut stocks = vec![&mut tuxedo];
    stocks.push(&mut ms);
    stocks[1].update_price(0.2);

    for stock in stocks {
        process_transfer(stock);
    }
}

fn main() {
    use_structs();
}
