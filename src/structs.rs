#[derive(Debug)]
pub struct Stock {
    name: String,
    open_price: f32,
    stop_loss: f32,
    take_profit: f32,
    current_price: f32,
}
impl Stock {
    pub fn new(stock_name: &str, price: f32) -> Stock {
        Stock {
            name: String::from(stock_name),
            open_price: price,
            stop_loss: 0.0,
            take_profit: 0.0,
            current_price: price,
        }
    }
    pub fn with_stop_loss(mut self, value: f32) -> Stock {
        self.stop_loss = value;
        self
    }
    pub fn with_take_profit(mut self, value: f32) -> Stock {
        self.take_profit = value;
        self
    }
    pub fn update_price(&mut self, value: f32) {
        self.current_price = value;
    }
}
pub trait CanTransfer {
    fn transfer(&mut self) -> ();

    fn print(&self) -> () {
        println!("A transfer:");
    }
}
impl CanTransfer for &mut Stock {
    fn transfer(&mut self) -> () {
        println!("stock {} transfered for  {}", self.name, self.current_price);
    }
}
pub fn process_transfer(mut stock: impl CanTransfer) -> () {
    stock.print();
    stock.transfer();
}
